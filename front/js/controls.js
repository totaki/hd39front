var getType = function(id) {
  var type;
  if (id.search("box") > -1) {
    type = "box";
  } else { 
    type = "sphere" 
  };
  return type;
}

var addObjectBlock = function(id) {
  var object = document.getElementById("accordion");
  object.appendChild(document.createElement("div"));
  object.lastChild.setAttribute("id", id);
  object.lastChild.classList.add("object");
  object.lastChild.appendChild(document.createElement("h4"));
  object.lastChild.lastChild.innerHTML = id;
  object.lastChild.lastChild.classList.add("accordion-toggle");
}

var getControls = function(id) {
  var object = document.getElementById(id);
  var controls = [controlPostition, controlScale];
  var mesh = scene.getMeshByID(id);
  for (var i=0; i < controls.length; i++) {
    object.appendChild(controls[i](mesh));
  }
};

var controlPostition = function(mesh) {
  var object = document.createElement("div");
  object.classList.add("position");
  var xyz = ["x", "y", "z"];
  for (var i=0; i < xyz.length; i++) {
    object.appendChild(document.createElement("label"));
    object.childNodes[i*2].innerHTML = xyz[i] + " ";
    object.appendChild(document.createElement("input"));
    object.lastChild.setAttribute("type","text");
    object.childNodes[i*2+1].classList.add(xyz[i]);
    object.childNodes[i*2+1].setAttribute("onchange","changeControl(this);");
    object.childNodes[i*2+1].value = mesh.position[xyz[i]];
  }
  return object;
};

var controlScale = function(mesh) {
  var object = document.createElement("div");
  object.classList
    .add("scale");
  object.appendChild(document.createElement("label"));
  object.childNodes[0].innerHTML = "Scale ";
  object.appendChild(document.createElement("input"));
  object.lastChild.setAttribute("type","text");
  object.childNodes[1].classList.add("scale");
  object.childNodes[1].setAttribute("onchange","changeControl(this);");
  object.childNodes[1].value = mesh.scaling.x;
  return object;
};

var getObjectClick = function(element) {
  this.name = 'Something Good';
  this.handleEvent = function(event) {
    var element = document.querySelectorAll("h4:hover");
    if (element.length > 0 && !element[0].parentElement.classList[1]){
      if (document.getElementsByClassName("active").length > 0) {
        var tempNode = document.getElementsByClassName("active")[0].childNodes[0]; 
        document.getElementsByClassName("active")[0].innerHTML = "";
        document.getElementsByClassName("active")[0].appendChild(tempNode);
        document.getElementsByClassName("active")[0].classList.remove("active");
      }
      element[0].parentElement.classList.add("active");
      getControls(element[0].parentElement.getAttribute("id"));
    }
  };
  element.addEventListener('click', this, false);
}

var changePosition = function(elem) {
  sendSocket({
    "action": "change", 
    "id": elem.parentElement.parentElement.getAttribute("id"),
    "type": getType(elem.parentElement.parentElement.getAttribute("id")),
    "data": stringElementData(
      changeElementPositionData(elem.parentElement.parentElement.getAttribute("id"), elem.classList[0], elem.value)
    )
  });
}

var changeScale = function(elem) {
  sendSocket({
    "action": "change", 
    "id": elem.parentElement.parentElement.getAttribute("id"),
    "type": getType(elem.parentElement.parentElement.getAttribute("id")),
    "data": stringElementData(
      changeElementScaleData(elem.parentElement.parentElement.getAttribute("id"), elem.value)
    )
  });
}

var changeControl = function(elem) {
  switch (elem.parentElement.classList[0]) {
    case "position":
      changePosition(elem);
      break;
    case "scale":
      changeScale(elem);
      break;
  }
}

getObjectClick(document.getElementsByClassName("elements")[0]);
