var canvas = document.getElementById('renderCanvas');

var engine = new BABYLON.Engine(canvas, true);

var createScene = function () {
	var scene = new BABYLON.Scene(engine);

	var camera = new BABYLON.ArcRotateCamera("Camera", 3 * Math.PI / 2, Math.PI / 8, 50, BABYLON.Vector3.Zero(), scene);

	camera.attachControl(canvas, true);

	var light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);

	return scene;
}

var scene = createScene();

var blueMaterial = new BABYLON.StandardMaterial("blue", scene);
blueMaterial.diffuseColor = new BABYLON.Color3(1, 0.92, 0.23);

var redMaterial = new BABYLON.StandardMaterial("red", scene);
redMaterial.diffuseColor = new BABYLON.Color3(0.13, 0.59, 0.95);

scene.clearColor = new BABYLON.Color4(0.0, 0.0, 0.0, 0.0);
//scene.clearColor = new BABYLON.Color3(0.9, 0.9, 0.85);

engine.runRenderLoop(function(){
    scene.render();
});
