var socket = new WebSocket("ws://10.6.40.250:8090/stocks?id=" + document.URL.split("?id="));

var currentMessage;

socket.onopen = function() {
  console.log("Соединение установлено.");
};

socket.onclose = function(event) {
  if (event.wasClean) {
    console.log('Соединение закрыто чисто');
  } else {
    console.log('Обрыв соединения');
  }
  console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

socket.onmessage = function(event) {
  action(JSON.parse(event.data));
};

socket.onerror = function(error) {
  console.log("Ошибка " + error.message);
};

var sendSocket = function(data) {
  socket.send(JSON.stringify(data));
}
