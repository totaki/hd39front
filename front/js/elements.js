var globalObjects = {"box": [], "sphere": []}

var elementData = function(data) {
  data = data.split("&");
  data = {
    "size": data[0].split(".").slice(1),
    "position": data[1].split(".").slice(1),
    "scale": data[2].split(".").slice(1)
  }
  return data;
}

var getElementData = function(id) {
  var element = scene.getMeshByID(id);
  var data = {
    "size": [5],
    "position": [element.position.x,element.position.y, element.position.z],
    "scale": [element.scaling.x]
  }
  return data;
}

var changeElementPositionData = function(id, axis, data) {
  var xyz = ["x","y","z"];
  var elem = getElementData(id);
  for (var i=0; i < xyz.length; i++) {
    if (xyz[i] === axis) {
      elem.position[i] = data;
    } 
  }
  return elem;
}

var changeElementScaleData = function(id, scale) {
  var elem = getElementData(id);
  elem.scale = scale;
  return elem;
}

var changeElementPosition = function(id, data) {
  var xyz = ["x","y","z"];
  var position = elementData(data)["position"];
  var scale = elementData(data)["scale"];
  var elem = scene.getMeshByID(id)
  for (var i=0; i < xyz.length; i++) {
    if (elem.position[xyz[i]] !== position[xyz[i]]) {
      elem.position[xyz[i]] = position[i];
    } 
  }
  if (elem.scaling.x !== scale) {
    for (var i=0; i < xyz.length; i++) {
      elem.scaling[xyz[i]] = scale;
    }
  } 
}

var stringElementData = function(data) {
  return "size." + data["size"] + "&position." + data["position"][0] + "." + data["position"][1] + "." + data["position"][2] + "&scale." + data["scale"]; 
}

var changeElementScale = function(id, scale) {
}

var createObject = function (type, data) {
  var funcs = {
      "box": function(data) {
        BABYLON.Mesh.CreateBox("box" + (globalObjects[type].length + 1), elementData(data)["size"][0], scene)
        changeElementPosition("box" + (globalObjects[type].length + 1), data);
        scene.getMeshByID("box" + (globalObjects[type].length + 1)).material = blueMaterial;
      },
      "sphere": function(data) {
        BABYLON.Mesh.CreateSphere("sphere" + (globalObjects[type].length + 1), 20, elementData(data)["size"][0], scene)
        changeElementPosition("sphere" + (globalObjects[type].length + 1), data);
        scene.getMeshByID("sphere" + (globalObjects[type].length + 1)).material = redMaterial;
      }
  }
  funcs[type](data);
  addObjectBlock(type + (globalObjects[type].length + 1));
  globalObjects[type].push(true);
}

var changeObject = function (id, data) {
  changeElementPosition(id, data)
}

var action = function(data) {
  var actions = {
    "add" : createObject,
    "change": changeObject
  }
  if (Array.isArray(data)) {
    for (var i=0; i < data.length; i++) {
      actions[data[i]["action"]](data[i]["type"] || data[i]["id"] , data[i]["data"]);
    }
  } else if (data["action"] === "change"){
    actions[data["action"]](data["id"], data["data"]);
  } else {
    actions[data["action"]](data["type"] , data["data"]);
  }
}


//Creation of a box
//(name of the box, size, scene)
//var box = BABYLON.Mesh.CreateBox("box", 6.0, scene);

//Creation of a sphere 
//(name of the sphere, segments, diameter, scene) 
//var sphere = BABYLON.Mesh.CreateSphere("sphere", 10.0, 10.0, scene);

//Creation of a plan
//(name of the plane, size, scene)
//var plan = BABYLON.Mesh.CreatePlane("plane", 10.0, scene);

//Creation of a cylinder
//(name, height, diameter, tessellation, scene, updatable)
//var cylinder = BABYLON.Mesh.CreateCylinder("cylinder", 3, 3, 3, 6, 1, scene, false);

// Creation of a torus
// (name, diameter, thickness, tessellation, scene, updatable)
//var torus = BABYLON.Mesh.CreateTorus("torus", 5, 1, 10, scene, false);

// Creation of a knot
// (name, radius, tube, radialSegments, tubularSegments, p, q, scene, updatable)
//var knot = BABYLON.Mesh.CreateTorusKnot("knot", 2, 0.5, 128, 64, 2, 3, scene);

// Creation of a lines mesh
/*
var lines = BABYLON.Mesh.CreateLines("lines", [
    new BABYLON.Vector3(-10, 0, 0),
    new BABYLON.Vector3(10, 0, 0),
    new BABYLON.Vector3(0, 0, -10),
    new BABYLON.Vector3(0, 0, 10)
], scene);
*/

// Creation of a ribbon
// let's first create many paths along a maths exponential function as an example 
/*
var exponentialPath = function (p) {
    var path = [];
    for (var i = -10; i < 10; i++) {
        path.push(new BABYLON.Vector3(p, i, Math.sin(p / 3) * 5 * Math.exp(-(i - p) * (i - p) / 60) + i / 3));
    }
    return path;
};
*/
// let's populate arrayOfPaths with all these different paths
/*
var arrayOfPaths = [];
for (var p = 0; p < 20; p++) {
    arrayOfPaths[p] = exponentialPath(p);
}
*/

// (name,  array of paths, closeArray, closePath, offset, scene)
/*
var ribbon = BABYLON.Mesh.CreateRibbon("ribbon", arrayOfPaths, false, false, 0, scene);
*/


// Moving elements
/*plan.position.z = 10;                            // Using a single coordinate component
cylinder.position.z = -10;
box.position = new BABYLON.Vector3(-10, 0, 0);   // Using a vector
sphere.position = new BABYLON.Vector3(0, 10, 0); // Using a vector
torus.position.x = 10;
knot.position.y = -10;
ribbon.position = new BABYLON.Vector3(-10, -10, 20);*/